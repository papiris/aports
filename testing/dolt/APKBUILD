# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dolt
pkgver=1.29.4
pkgrel=0
pkgdesc="Dolt – It's Git for Data"
url="https://www.dolthub.com"
arch="all !x86 !armhf !armv7" # fails on 32-bit
license="Apache-2.0"
options="!check chmod-clean net"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/dolthub/dolt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/go"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir -p build
	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\"" \
		-o build \
		./cmd/...
}

package() {
	install -Dm755 build/dolt "$pkgdir"/usr/bin/dolt
}

sha512sums="
c7e6dc6703d04896830aeeb0108a3c1fe258f577a3336de4cf95d6b59e771aeb11331960e49baa23856f5c78e51b973f4c9120e8636a431d1a64942160ca5164  dolt-1.29.4.tar.gz
"
