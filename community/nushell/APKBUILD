# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.88.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x: nix crate
arch="all !s390x"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	sqlite-dev
	"
checkdepends="bash"
subpackages="$pkgname-doc $pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz
	system-deps.patch
	"

case "$CARCH" in
# ooms when building
armhf|armv7|ppc64le|riscv64|x86) _exclude_opts="--exclude nu-cmd-dataframe" ;;
esac

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF


		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		rusqlite = { rustc-link-lib = ["sqlite3"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --workspace --release --frozen $_exclude_opts
}

check() {
	cargo test --workspace --frozen $_exclude_opts
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;

	install -Dm644 LICENSE \
		-t "$pkgdir"/usr/share/licenses/$pkgname/
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell=$pkgver-r$pkgrel"

	amove usr/bin/nu_plugin_*
}

sha512sums="
fd56ef810690f4b1b7a8973ec770cfc84b8f066e873ff0b613be4a6626092a54f3bfdeb1aaeb5603961c0d9b2ba28934b17bb21d7b7d4a9eb8ed00c3822ebe4e  nushell-0.88.0.tar.gz
cd95107d6160d042542c4e97b3238ad6eb38ab45c8d6a390c028181dae6e02c4c129c5e019a6210551bb3d0c4c690bc65823377f176a4d78128f48cf860bda61  system-deps.patch
"
