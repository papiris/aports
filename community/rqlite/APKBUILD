# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=rqlite
pkgver=8.0.6
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all !riscv64" # ftbfs
license="MIT"
makedepends="go sqlite-dev"
checkdepends="python3 py3-requests"
options="chmod-clean !check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench &
	go build ./cmd/rqlite &
	go build ./cmd/rqlited &
	wait
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin/

	install -Dm644 DOC/*.md \
		-t "$pkgdir"/usr/share/doc/$pkgname/

	install -Dm644 LICENSE \
		-t "$pkgdir"/usr/share/licenses/$pkgname/
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}

client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
2442f56deb30916e2e85639b048ab8d5c61a679a7a8e6040c44df9a0985cbccfa5911881d062959c13dbea28e0ff063d9fa21b216fe36aa54a65c68efb708b06  rqlite-8.0.6.tar.gz
"
